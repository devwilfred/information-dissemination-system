import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {AngularFireDatabase, snapshotChanges} from '@angular/fire/database';
import {AngularFireAuth} from '@angular/fire/auth';
import {FormGroup} from '@angular/forms';
import {User} from './common/models';
import {LocalStorage} from '@ngx-pwa/local-storage';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private _isLoggedIn = new BehaviorSubject(false);

    readonly isLoggedIn$ = this._isLoggedIn.asObservable();

    constructor(private db: AngularFireDatabase, private storage: LocalStorage, private auth: AngularFireAuth) {
    }

    publishLogIn(bool: boolean) {
        this._isLoggedIn.next(bool);
    }

    userExists(email: string) {
        return this.db.list('users').query.orderByValue().equalTo(email)
            .once('value');
    }

    addUser(form: FormGroup) {
        return this.auth.auth.createUserWithEmailAndPassword(form.value['email'], form.value['password']);
    }

    authUser(form: FormGroup) {
        return this.auth.auth.signInWithEmailAndPassword(form.value['email'], form.value['password']);
    }

    fetchUserDetails(uid: string) {
        return this.db.object(`users/${uid}`).query.once('value');
    }

    addUserToDb(user: User) {
        return this.db.database.ref(`users`).child(user.uid).set(user);
    }

    logOut() {
        this.storage.clearSubscribe();
        return this.auth.auth.signOut();
    }
}
