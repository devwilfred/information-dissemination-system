import {Component, HostListener} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {FirebaseApp} from '@angular/fire';
import * as firebase from 'firebase';
import {AngularFireAuth} from '@angular/fire/auth';
import {AuthService} from './auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'information-dissemination';

    showElevation = false;


    constructor(private auth: AngularFireAuth, private authService: AuthService) {
        this.auth.authState.subscribe(u => {
            if (u) {
                this.authService.publishLogIn(true);
            } else {
                this.authService.publishLogIn(false);
            }
        });
    }

    @HostListener('window:scroll', []) onScroll(event) {
        this.showElevation = true;
        this.showElevation = !(this.showElevation && window.scrollY < 10);
    }
}
