import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {AuthService} from '../auth.service';
import {MatDialog} from '@angular/material';
import {AuthComponent} from '../auth/auth.component';
import {LocalStorage} from '@ngx-pwa/local-storage';


@Component({
    selector: 'app-holder',
    templateUrl: './holder.component.html',
    styleUrls: ['./holder.component.scss']
})
export class HolderComponent implements OnInit {
    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
        .pipe(
            map(result => result.matches)
        );

    isLoggedIn$: Observable<boolean>;
    collapsed = new BehaviorSubject<boolean>(false);
    navLinks = [
        /*{
            label: 'Dashboard',
            url: 'dashboard',
            icon: 'pie_chart_outlined'
        },*/
        {
            label: 'Shuttle Service',
            url: 'shuttle',
            icon: 'directions_car',
            children: [
                {
                    label: 'Overview',
                    url: 'shuttle/overview',
                },
                {
                    label: 'Manage trips',
                    url: 'shuttle/manage',
                }
            ]
        },
        {
            label: 'Rental Management',
            url: 'rental',
            icon: 'credit_card',
            children: [
                {
                    label: 'Overview',
                    url: 'rental/overview',
                },
                {
                    label: 'Unconfirmed rents',
                    url: 'rental/unconfirmed',
                },
                {
                    label: 'Confirmed rents',
                    url: 'rental/confirmed',
                }
            ]
        },
        {
            label: 'Organizer Management',
            url: 'organizer',
            icon: 'people_outline',
            children: [
                {
                    label: 'Overview',
                    url: 'organizer-overview',
                },
                {
                    label: 'Organized trips',
                    url: 'organized-trips',
                }
            ]
        },
        {
            label: 'User Management',
            url: 'users',
            icon: 'person_outline',
            children: [
                {
                    label: 'Overview',
                    url: 'users-overview',
                },
                {
                    label: 'Manage users',
                    url: 'manage-users',
                }
            ]
        }
    ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private breakpointObserver: BreakpointObserver,
        private auth: AuthService,
        private dialog: MatDialog,
        private storage: LocalStorage) {
        this.isLoggedIn$ = this.auth.isLoggedIn$;

        this.storage.getItem('user').subscribe(u => console.log(u));
    }

    collapseNav() {
        this.collapsed.next(!this.collapsed.value);
    }

    logOut() {
        this.auth.logOut().catch(e => console.log(e))
            .then(() => this.auth.publishLogIn(false));
    }

    authUser(regOrLogin: string) {
        this.dialog.open(AuthComponent, {
            width: regOrLogin === 'Register' ? '600px' : '300px',
            closeOnNavigation: true,
            panelClass: 'round-corner',
            data: {type: regOrLogin}
        });
    }

    ngOnInit(): void {
    }


}
