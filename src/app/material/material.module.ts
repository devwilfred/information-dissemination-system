import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
    MatButtonModule,
    MatCardModule,
    MatDialogModule, MatIconModule, MatInputModule,
    MatListModule, MatMenuModule,
    MatProgressBarModule, MatSelectModule, MatSidenavModule,
    MatSnackBarModule,
    MatToolbarModule
} from '@angular/material';

@NgModule({
    declarations: [],
    exports: [
        CommonModule,
        MatSnackBarModule,
        MatButtonModule,
        MatListModule,
        MatCardModule,
        MatProgressBarModule,
        MatDialogModule,
        MatToolbarModule,
        MatMenuModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule
    ]
})
export class MaterialModule {
}
