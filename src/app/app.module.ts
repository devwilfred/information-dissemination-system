import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material/material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import { HolderComponent } from './holder/holder.component';
import { HomeComponent } from './home/home.component';
import {RoutingModule} from './routing/routing.module';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import { AuthComponent } from './auth/auth.component';
import {AngularFireAuthModule} from '@angular/fire/auth';

@NgModule({
    declarations: [
        AppComponent,
        HolderComponent,
        HomeComponent,
        AuthComponent
    ],
    entryComponents: [
        AuthComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        RoutingModule,
        ReactiveFormsModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        MaterialModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
