import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {FormBuilder, Validators} from '@angular/forms';
import {StateMatcher} from '../common/util';
import {SelectData, User} from '../common/models';
import {environment} from '../../environments/environment';
import {AuthService} from '../auth.service';
import {LocalStorage} from '@ngx-pwa/local-storage';


interface DialogData {
    type: string;
}

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
    errorStateMatcher = new StateMatcher();
    logInForm = this.fb.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required]]
    });

    register = this.fb.group({
        email: ['test.asomani@gmail.com', [Validators.required, Validators.email]],
        name: ['asd', [Validators.required]],
        year: ['year 4', [Validators.required]],
        faculty: ['foe', [Validators.required]],
        department: ['computer', [Validators.required]],
        hostel: ['Hilda', Validators.required],
        password: ['wilfred123', [Validators.required]]
    });

    hostels: SelectData[] = environment.hostels;
    departments: SelectData[] = environment.departments;
    year_groups: SelectData[] = environment.year_groups;
    faculties: SelectData[] = environment.faculty;

    user: User;

    constructor(
        public dialogRef: MatDialogRef<AuthComponent>,
        private snackBar: MatSnackBar,
        private fb: FormBuilder,
        private auth: AuthService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private storage: LocalStorage) {
    }

    ngOnInit() {
    }

    authenticate() {
        switch (this.data.type) {
            case 'Register':
                this.auth.addUser(this.register)
                    .catch(e => console.log(e))
                    .then(res => {
                        res.user.updateProfile({displayName: this.register.value['name'], photoURL: null})
                            .catch(e => console.log(e))
                            .then(() => {
                                this.snackBar.open('Registered successfully', '', {duration: 2000});
                                this.user = {
                                    name: this.register.value['name'],
                                    email: this.register.value['email'],
                                    year: this.register.value['year'],
                                    department: this.register.value['year'],
                                    faculty: this.register.value['year'],
                                    uid: res.user.uid
                                };
                                this.storage.setItemSubscribe('user', this.user);
                                this.auth.addUserToDb(this.user)
                                    .then(v => {
                                        this.auth.publishLogIn(true);
                                        this.snackBar.open('Authenticated successfully', '', {duration: 3000});
                                        this.dialogRef.close();
                                        // console.log(v);
                                    });
                            });
                    });
                break;
            case 'Log In':
                this.auth.authUser(this.logInForm).catch(e => console.log(e))
                    .then(r => {
                        this.auth.fetchUserDetails(r.user.uid).catch(e => console.log(e))
                            .then(a => {
                                this.user = a.val();
                                this.storage.setItemSubscribe('user', a.val());
                                this.snackBar.open(`Welcome ${this.user.name}`, '', {duration: 3000});
                                this.dialogRef.close();
                                this.auth.publishLogIn(true);
                                // console.log(a.val());
                            });
                        // console.log(r);
                    });
                break;
        }
    }
}
