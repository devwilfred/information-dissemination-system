export interface User {
    name: string;
    email: string;
    photo?: string;
    year: string;
    department: string;
    faculty: string;
    uid: string;
}

export interface Announcement {
    subject: string;
    body: string;
    tags: string[];
}

export interface SelectData {
    name: string;
    value: string;
}