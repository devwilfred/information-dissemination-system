// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: 'AIzaSyCbZJgkiADwmD2jqXWTaqnZg5AdhbtiOMw',
        authDomain: 'info-dissemination-system.firebaseapp.com',
        databaseURL: 'https://info-dissemination-system.firebaseio.com',
        projectId: 'info-dissemination-system',
        storageBucket: 'info-dissemination-system.appspot.com',
        messagingSenderId: '970242499808'
    },
    hostels: [
        {
            name: 'Kofi Tetteh',
            value: 'Kofi Tetteh'
        },
        {
            name: 'Hilda',
            value: 'Hilda'
        },
        {
            name: 'Chamber of Mines',
            value: 'Chamber of Mines'
        },
        {
            name: 'Camp City',
            value: 'Camp City'
        },
        {
            name: 'Gold Refinery',
            value: 'Gold Refinery'
        },
    ],
    year_groups: [
        {name: 'Year 1', value: 'year 1'},
        {name: 'Year 2', value: 'year 2'},
        {name: 'Year 3', value: 'year 3'},
        {name: 'Year 4', value: 'year 4'}
    ],
    departments: [
        {name: 'Computer', value: 'computer'},
        {name: 'Electrical', value: 'electrical'},
        {name: 'Environmental', value: 'environmental'},
        {name: 'Mechanical', value: 'mechanical'}
    ],
    faculty: [
        {name: 'Engineering', value: 'foe'},
        {name: 'Mineral', value: 'fmrt'}
    ]
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
