export const environment = {
    production: true,
    firebaseConfig: {
        apiKey: 'AIzaSyCbZJgkiADwmD2jqXWTaqnZg5AdhbtiOMw',
        authDomain: 'info-dissemination-system.firebaseapp.com',
        databaseURL: 'https://info-dissemination-system.firebaseio.com',
        projectId: 'info-dissemination-system',
        storageBucket: 'info-dissemination-system.appspot.com',
        messagingSenderId: '970242499808'
    },
    hostels: [
        {
            name: 'Kofi Tetteh',
            value: 'Kofi Tetteh'
        },
        {
            name: 'Hilda',
            value: 'Hilda'
        },
        {
            name: 'Chamber of Mines',
            value: 'Chamber of Mines'
        },
        {
            name: 'Camp City',
            value: 'Camp City'
        },
        {
            name: 'Gold Refinery',
            value: 'Gold Refinery'
        }
    ],
    year_groups: [
        {name: 'Year 1', value: 'year 1'},
        {name: 'Year 2', value: 'year 2'},
        {name: 'Year 3', value: 'year 3'},
        {name: 'Year 4', value: 'year 4'}
    ],
    departments: [
        {name: 'Computer', value: 'computer'},
        {name: 'Electrical', value: 'electrical'},
        {name: 'Environmental', value: 'environmental'},
        {name: 'Mechanical', value: 'mechanical'}
    ],
    faculty: [
        {name: 'Engineering', value: 'foe'},
        {name: 'Mineral', value: 'fmrt'}
    ]
};
